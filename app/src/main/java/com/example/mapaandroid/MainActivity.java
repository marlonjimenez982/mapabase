package com.example.mapaandroid;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

public class MainActivity extends AppCompatActivity {

    MapView mapa;
    IMapController mapControlador;
    MyLocationNewOverlay miubicacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mapa=(MapView) findViewById(R.id.mapa);
        //paso1 contener contexto vista
        Context ctx = getApplicationContext();

        //paso2 configuracion
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        //paso3 Tile por partes
        mapa.setTileSource(TileSourceFactory.MAPNIK);

        mapa.setMultiTouchControls(true);//usar dedos del touch

         //pedir permisos sino funciona
        String[] permisos= new String[]{

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            requestPermissions(permisos,1);
        }

        //COLOCAR MARCADOR DEL GPS
        mapControlador=mapa.getController();
        miubicacion= new MyLocationNewOverlay(new GpsMyLocationProvider(this),mapa);
        //habilitar la actualización al moverse
        miubicacion.enableMyLocation();

        //agregar señal al mapa
        mapa.getOverlays().add(miubicacion);

        //FUNCIONES LANDA
        miubicacion.runOnFirstFix(
                ()->{
                    runOnUiThread(
                            ()->{
                                mapControlador.setZoom((double)15);
                                mapControlador.setCenter(miubicacion.getMyLocation());
                                Log.d(TAG, "Ubicacion!!" + miubicacion.getMyLocation().getLatitude()+miubicacion.getMyLocation().getLongitude());

                            });
                }
        );





        /*
        //TRABAJO INICIAL CARGANDO LA UBICACION MANUAL
        //marcador en el mapa con ubicacion
        GeoPoint miPunto=new GeoPoint(4.62 , -74.16); //PUNTO ESPECIFICO
        Marker mimarcador = new Marker(mapa);
        mimarcador.setPosition(miPunto);
        mapa.getOverlays().add(mimarcador);

        //marcador en el mapa con ubicacion
        GeoPoint miPunto2=new GeoPoint(4.64 , -74.12); //PUNTO ESPECIFICO
        Marker mimarcador2 = new Marker(mapa);
        mimarcador2.setPosition(miPunto2);
        mapa.getOverlays().add(mimarcador2);


        //controlar el mapa

        mapControlador=mapa.getController();
        mapControlador.setZoom(13.0); //Tamaño del zoom 0 a 20.0
        mapControlador.setCenter(miPunto2);
        //mapControlador.animateTo(miPunto);//animacion
        */

    }
}